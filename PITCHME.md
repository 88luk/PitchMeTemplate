# Scrum Intro
> Es ist gut einen Plan zu haben.
> Besser ist es wenn der Plan auch angepasst werden kann. 

+++

![Logo](assets/octocat.jpg)

---

## Scrum Team

* 1 Product Owner
* 1 Scrum Master
* 3-9 Entwickler (Entwickler = Tester!)

![Scrum Team](https://openclipart.org/download/272476/working-team.svg)

---

## Ereignisse
* Sprint
* Sprint Planning
* Daily Scrum
* Sprint Review
* Sprint Retrospektive

---

Links:

* (GitLabRepo)[https://88luk@gitlab.com/88luk/ScrumIntro.git]

* (Präsentation)[https://gitpitch.com/88luk/ScrumIntro/work?grs=gitlab&t=moon]

---

The end!
